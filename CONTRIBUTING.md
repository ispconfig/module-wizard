# Contributing

Contributions are welcome. Our release model uses [Git Flow approach](http://danielkummer.github.io/git-flow-cheatsheet/), so you have to base your branches on `develop` and should have name `feature/name-of-feature` or `hotfix/name-of-hotfix`.

[How to fork a repo in Gitlab](https://docs.gitlab.com/ce/workflow/forking_workflow.html)

Please contribute your translations, which are located in `lib/lang/` directory. Thank you.

## Example of git commands to use

```
git clone https://git.ispconfig.org/ispconfig/module-wizard.git
cd module-wizard
git checkout -b feature/spanish-translation origin/develop
```

Do some work, commit changes and push to server

```
git add .
git commit -am "Added Spanish translation"
git push -u origin feature/spanish-translation
```